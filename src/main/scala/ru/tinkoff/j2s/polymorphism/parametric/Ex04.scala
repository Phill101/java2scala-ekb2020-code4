package ru.tinkoff.j2s.polymorphism.parametric

object MagmaInstances {
  val magmaInt = new Magma[Int] {
    def op(x: Int, y: Int): Int = x - y
  }
  val magmaString = new Magma[String] {
    def op(x: String, y: String): String = x + " and " + y
  }
}
