package ru.tinkoff.j2s.polymorphism.parametric

import Functor._
import ValidatedFunctor._
import Validated._
import Semigroup._

object ApplicativeInstances {
  implicit val applicativeOption = new Applicative[Option] {
    override def map[A, B](fa: Option[A])(f: A => B): Option[B] = 
      fa.map(f)
    override def pure[A](a: A): Option[A] = 
      Option(a)
    override def product[A, B](fa: Option[A], fb: Option[B]): Option[(A, B)] = 
      fa.zip(fb)
  }

  implicit def applicativeValidated[E: Semigroup] = new Applicative[Validated[E, *]] {
    override def map[A, B](fa: Validated[E,A])(f: A => B): Validated[E,B] = 
      fa.map(f)
    override def pure[A](a: A): Validated[E,A] = 
      Valid(a)
    override def product[A, B](fa: Validated[E,A], fb: Validated[E,B]): Validated[E,(A, B)] =
      (fa, fb) match {
        case (Valid(a)   , Valid(b)   ) => Valid((a, b))
        case (Invalid(e1), Invalid(e2)) => Invalid(e1 |+| e2)
        case (_          , Invalid(e2)) => Invalid(e2)
        case (Invalid(e1), _          ) => Invalid(e1)
      }
  }
}
