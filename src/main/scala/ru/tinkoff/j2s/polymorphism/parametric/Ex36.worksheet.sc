import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class User(id: Int, name: String)
object User {
  case class Id(id: Int)// extends AnyVal
}

def getUser(id: User.Id): Future[User] = 
  Future.successful(User(id.id, s"user$id"))


def getUsers(ids: List[User.Id]): Future[List[User]] =
  Future.traverse(ids)(getUser)


getUser(User.Id(1))
getUsers(List.range(1, 10).map(User.Id))
