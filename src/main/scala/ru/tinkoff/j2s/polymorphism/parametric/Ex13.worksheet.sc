import ru.tinkoff.j2s.polymorphism.parametric.Semigroup
import ru.tinkoff.j2s.polymorphism.parametric.Semigroup._

val wrongSemigroup = new Semigroup[Int] {
  override def combine(x: Int, y: Int): Int = x - y
}
implicit val okSemigroup = new Semigroup[Int] {
  override def combine(x: Int, y: Int): Int = x + y
}







val list = (1 to 5).toList
val (left, right) = list.splitAt(2)

val combinedLeft  = left.foldLeft(0)(_ |+| _)
val combinedRight = right.foldLeft(0)(_ |+| _)

val result = combinedLeft |+| combinedRight
list.foldLeft(0)(_ |+| _)
