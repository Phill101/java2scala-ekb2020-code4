package ru.tinkoff.j2s.polymorphism.parametric

trait Applicative[F[_]] extends Functor[F] {
  // pure(()).product(fa) ~ fa
  // fa.product(pure(())) ~ fa
  // fa.product(fb).product(fc) ~ fa.product(fb.product(fc))
  def pure[A](a: A): F[A]
  def product[A, B](fa: F[A], fb: F[B]): F[(A, B)]

  // def ap[A, B](ff: F[A => B])(fa: F[A]): F[B]
  // override def map[A, B](fa: F[A])(f: A => B): F[B] = ap(pure(f))(fa)

  def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    map(product(fa, fb))(f.tupled)
}

object Applicative {
  def apply[F[_]: Applicative]: Applicative[F] = implicitly
}
