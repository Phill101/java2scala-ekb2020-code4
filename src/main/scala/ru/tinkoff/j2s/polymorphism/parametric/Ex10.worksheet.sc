import ru.tinkoff.j2s.polymorphism.parametric.Magma
import ru.tinkoff.j2s.polymorphism.parametric.Magma.instances._

def function[A: Magma](first: A, second: A): Unit = {
  println("received: " + first.toString + " " + second.toString)
  val r1 = Magma[A].op(first, second)
  val r2 = Magma[A].op(second, first)
  println("r1: " + r1)
  println("r2: " + r2)
}

function(47, 5)
function("s1", "s2")
