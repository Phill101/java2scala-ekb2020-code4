import ru.tinkoff.j2s.polymorphism.parametric.Semigroup
import ru.tinkoff.j2s.polymorphism.parametric.Semigroup._

def combineAll[A: Semigroup](xs: List[A]): A =
  xs.foldLeft(???)(_ |+| _)
