package ru.tinkoff.j2s.polymorphism.parametric

import ru.tinkoff.j2s.polymorphism.parametric.Validated
import ru.tinkoff.j2s.polymorphism.parametric.Validated._

object ValidatedFunctor {
  implicit def validatedFunctor[E] =
    // val someB: B
    //
    // val f: (A, B) => C
    // val g: A => C = 
    //   a => f(a, someB)
    new Functor[({type F[A] = Validated[E, A]})#F] {
      override def map[A, B](fa: Validated[E, A])(f: A => B): Validated[E, B] = fa match {
        case Valid(a)       => Valid(f(a))
        case i @ Invalid(_) => i
      }
    }
}
