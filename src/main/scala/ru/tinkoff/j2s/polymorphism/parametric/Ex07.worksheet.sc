import ru.tinkoff.j2s.polymorphism.parametric.{Magma, MagmaInstances}

def function[A](first: A, second: A)(implicit magma: Magma[A]): Unit = {
  println("received: " + first.toString + " " + second.toString)
  val r1 = magma.op(first, second)
  val r2 = magma.op(second, first)
  println("r1: " + r1)
  println("r2: " + r2)
}

implicit val mi = MagmaInstances.magmaInt
implicit val ms = MagmaInstances.magmaString


function(47, 5)
function("hello", "world")
