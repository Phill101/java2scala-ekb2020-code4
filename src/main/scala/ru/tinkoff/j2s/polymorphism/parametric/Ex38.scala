package ru.tinkoff.j2s.polymorphism.parametric

import cats.Applicative

trait Tree[A] {
  def traverse[F[_]: Applicative, B](f: A => F[B]): F[Tree[B]] = this match {
    case Tree.Empty()         => Applicative[F].pure(Tree.Empty())
    case Tree.Branch(v, l, r) => Applicative[F].map3(f(v), l.traverse(f), r.traverse(f))(Tree.Branch(_, _, _))
  }
}

object Tree {
  case class Empty[A]() extends Tree[A]
  case class Branch[A](value: A, left: Tree[A], right: Tree[A]) extends Tree[A]
}
