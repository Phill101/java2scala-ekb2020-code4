import cats._
import cats.instances.all._
import cats.syntax.all._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

def sumSquare[F[_]: Monad](a: F[Int], b: F[Int]): F[Int] =
  for {
    x <- a
    y <- b
  } yield x*x + y*y

sumSquare(Option(1), Option(2))
sumSquare(List(1, 2, 3), List(4, 5))
sumSquare[Id](1, 2)


val f = Future.successful(42 > 40)
              .ifA(
                ifTrue  = Future(println("всё так")),
                ifFalse = 
                  Future(println("проверяем положение планет")) >>
                  Future.failed(new Exception("Положение планет неподходящее"))
                        .whenA(40 > 42) >>
                  Future(println("штош"))
              )
