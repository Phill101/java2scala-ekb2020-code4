package ru.tinkoff.j2s.polymorphism.parametric

object Sl2 {
  class Box[A](val a: A)
  def two[T](a: T, b: T) = a :: b :: Nil

  abstract class Animal {
    def say: String
  }
  class Dog extends Animal {
    override def say: String = "Bark"
  }
  class Cat extends Animal {
    override def say: String = "Meow"
  }
}
