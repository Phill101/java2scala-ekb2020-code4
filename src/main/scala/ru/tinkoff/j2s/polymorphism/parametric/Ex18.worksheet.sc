import cats.Monoid
import cats.instances.int._
import cats.instances.string._
import cats.syntax.semigroup._

def foldAnything[A: Monoid](xs: List[A]): A =
  xs.foldLeft(Monoid[A].empty)(_ |+| _)

foldAnything(5 :: 10 :: 15 :: Nil)
foldAnything("hello" :: " " :: "world" :: Nil)
