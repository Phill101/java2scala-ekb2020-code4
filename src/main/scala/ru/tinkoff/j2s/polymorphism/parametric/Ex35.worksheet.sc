import cats.Applicative
import cats.data.{NonEmptyList, Validated}
import cats.data.Validated._
import cats.syntax.all._

type ValidationResult[A] = Validated[NonEmptyList[String], A]

def validateUserName(userName: String): ValidationResult[String] =
  if(userName.matches("^[a-zA-Z0-9]+$")) Valid(userName)
  else Invalid(NonEmptyList.one("Username cannot contain special characters."))


def validateAge(age: Int): ValidationResult[Int] =
  if(age >= 18 && age <= 75) Valid(age)
  else Invalid(NonEmptyList.one("You must be aged 18 and not older than 75 to use our services."))

validateUserName("Joe")
validateUserName("Joe%%%")

validateAge(21)
validateAge(99)

case class RegistrationData(userName: String, age: Int)
def validateForm(userName: String, age: Int): ValidationResult[RegistrationData] =
  Applicative[ValidationResult].map2(
    validateUserName(userName),
    validateAge(age)
  )((a, b) => RegistrationData(a, b))

validateForm("Joe", 21)
validateForm("Joe%%%", 21)
validateForm("Joe", 99)
validateForm("Joe%%%", 99)
