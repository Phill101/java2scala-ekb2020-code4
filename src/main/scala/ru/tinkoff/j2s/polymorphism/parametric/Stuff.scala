package ru.tinkoff.j2s.polymorphism.parametric

object Stuff {
  implicit def appliativeId[A] = new Applicative[IdMonad.Id] {
    override def map[A, B](fa: IdMonad.Id[A])(f: A => B): IdMonad.Id[B] = f(fa)
    override def pure[A](a: A): IdMonad.Id[A] = a
    override def product[A, B](fa: IdMonad.Id[A], fb: IdMonad.Id[B]): IdMonad.Id[(A, B)] = (fa, fb)
  }
}
