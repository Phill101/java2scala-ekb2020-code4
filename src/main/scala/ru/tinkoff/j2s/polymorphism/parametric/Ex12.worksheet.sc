import ru.tinkoff.j2s.polymorphism.parametric.Semigroup
import ru.tinkoff.j2s.polymorphism.parametric.Semigroup._

val wrongSemigroup = new Semigroup[Int] {
  override def combine(x: Int, y: Int): Int = x - y
}
implicit val okSemigroup = new Semigroup[Int] {
  override def combine(x: Int, y: Int): Int = x + y
}

Semigroup.associativity(5, 10, 15)(wrongSemigroup)
Semigroup.associativity(5, 10, 15)(okSemigroup)

5 |+| 6
