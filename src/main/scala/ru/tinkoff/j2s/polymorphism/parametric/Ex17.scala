package ru.tinkoff.j2s.polymorphism.parametric

import ru.tinkoff.j2s.polymorphism.parametric.Monoid
import ru.tinkoff.j2s.polymorphism.parametric.Semigroup._

object SemigroupInstances {
  implicit val intSemigroup = new Semigroup[Int] {
    override def combine(x: Int, y: Int): Int = x + y
  }
  implicit val stringSemigroup = new Semigroup[String] {
    override def combine(x: String, y: String): String = x + y
  }
}

object MonoidInstances {
  import SemigroupInstances._

  implicit val intMonoid = new Monoid[Int] {
    override def combine(x: Int, y: Int): Int = x |+| y
    override def empty: Int = 0
  }

  implicit val stringMonoid = new Monoid[String] {
    override def combine(x: String, y: String): String = x |+| y
    override def empty: String = ""
  }

  implicit def optionMonoid[A: Monoid] = new Monoid[Option[A]] {
    override def combine(x: Option[A], y: Option[A]): Option[A] = x match {
      case None     => y
      case Some(xv) =>
        y match {
          case None     => x
          case Some(yv) => Some(xv |+| yv)
        }
    }
    override def empty: Option[A] = None
  }
}
