package ru.tinkoff.j2s.polymorphism.parametric.shadowed

trait Magma[A] {
  def op(x: A, y: A): A
}
