import cats.Functor
import cats.instances.option._
import cats.instances.list._
import cats.syntax.functor._

import scala.util.matching.Regex._

case class MaskedCardNumber private (value: String) //extends AnyVal
object MaskedCardNumber {
  def apply(unmasked: String): MaskedCardNumber =
    new MaskedCardNumber(num.replaceAllIn(unmasked, mask))

  private val num  = """(\d{4})(\d{8})(\d{4})""".r
  private val mask: Match => String = {
    case Groups(prefix, _, suffix) => prefix + "********" + suffix
  }
}



def maskCardNumberOccurences[F[_]: Functor](cn: F[String]): F[MaskedCardNumber] =
  cn.map(MaskedCardNumber(_))


val unmasked: String = "receive 1234000000005678 and 1111999999991111. they should be masked"

maskCardNumberOccurences(Option(unmasked))
maskCardNumberOccurences(List.fill(3)(unmasked)).mkString("List(\n  ","\n  ", "\n)")

def maskCardNumber[F[_]: Functor](cn: F[Long]): F[MaskedCardNumber] =
  maskCardNumberOccurences(cn.map(_.toString))

maskCardNumber(1234000000005678L :: 1111999999991111L :: Nil)
maskCardNumber(Option(1234000000005678L))

