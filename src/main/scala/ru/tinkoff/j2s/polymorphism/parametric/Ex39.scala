package ru.tinkoff.j2s.polymorphism.parametric

import ru.tinkoff.j2s.polymorphism.parametric.Stuff.appliativeId

trait Traverse[F[_]] extends Functor[F] {
  def traverse[G[_]: Applicative, A, B](fa: F[A])(f: A => G[B]): G[F[B]]

  def sequence[G[_]: Applicative, A](fga: F[G[A]]): G[F[A]] =
    traverse(fga)(identity)

  override def map[A, B](fa: F[A])(f: A => B): F[B] =
    traverse[IdMonad.Id, A, B](fa)(f)
}

object Traverse {
  def apply[F[_]: Applicative]: Applicative[F] = implicitly
}
