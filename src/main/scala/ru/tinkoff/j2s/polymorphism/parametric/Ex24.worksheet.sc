Option(1).flatMap(i => Option(i.toString + "!"))
Option(1).flatMap(i => None)
None.flatMap(i => Option(i.toString + "!"))
None.flatMap(i => None)


Right[String, Int](1).flatMap(i => Right(i + 1))
Right[String, Int](1).flatMap(i => Left("nope"))
Left[String, Int]("nope").flatMap(i => Right(i + 1))
Left[String, Int]("nope").flatMap(i => Left("also nope"))
