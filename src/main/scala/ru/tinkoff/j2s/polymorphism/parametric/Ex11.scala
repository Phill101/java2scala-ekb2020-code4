package ru.tinkoff.j2s.polymorphism.parametric

trait Semigroup[A] {
  // (x |+| (y |+| z)) === ((x |+| y) |+| z)
  def combine(x: A, y: A): A
}

object Semigroup {
  def apply[A](implicit s: Semigroup[A]) = s

  def associativity[A: Semigroup](x: A, y: A, z: A): Boolean =
    (x |+| (y |+| z)) == ((x |+| y) |+| z)
    //val s = Semigroup[A]
    //s.combine(x, s.combine(y, z)) == s.combine(s.combine(x, y), z)

  implicit class SemigroupOps[A](val x: A) extends AnyVal {
    def |+|(y: A)(implicit s: Semigroup[A]) = s.combine(x, y)
  }
}
