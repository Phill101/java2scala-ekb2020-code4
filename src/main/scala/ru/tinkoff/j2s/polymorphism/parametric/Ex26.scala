package ru.tinkoff.j2s.polymorphism.parametric

object MonadInstances {
  import Monad._
  import Functor._
  import FunctorInstances._

  implicit val optionMonad = new Monad[Option] {
    override def pure[A](a: A): Option[A] = Some(a)
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] =
      fa match {
        case Some(a) => f(a)
        case None    => None
      }

    override def map[A, B](fa: Option[A])(f: A => B): Option[B] = 
      Functor[Option].map(fa)(f)
  }
}
