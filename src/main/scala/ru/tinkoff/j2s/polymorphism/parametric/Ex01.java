package ru.tinkoff.j2s.polymorphism.parametric;

import java.util.ArrayList;

public class Ex01 {
  public static class Box<A> {
    private A value;

    public Box(A a) {
      this.value = a;
    }
    public A getValue() {
      return value;
    }
  }
  public static <T> ArrayList<T> two(T a, T b) {
    var arr = new ArrayList<T>();
    arr.add(a);
    arr.add(b);
    return arr;
  }

  public static abstract class Animal {
    public abstract String say();
  }
  public static class Dog extends Animal {
    @Override
    public String say() {
      return "Bark";
    }
  }
  public static class Cat extends Animal {
    @Override
    public String say() {
      return "Meow";
    }
  }
}
