import cats.Applicative
import cats.instances.option._

def traverse[F[_]: Applicative, A, B](as: List[A])(f: A => F[B]): F[List[B]] =
  as.foldRight(Applicative[F].pure(List.empty[B])) { (a: A, acc: F[List[B]]) =>
    val fb: F[B] = f(a)
    Applicative[F].map2(fb, acc)(_ :: _)
  }

def func(int: Int): Option[String] = Option(int.toString.reverse)

traverse(List.range(1, 10))(func)
