import ru.tinkoff.j2s.polymorphism.parametric.{Magma, MagmaInstances}

def function[A](first: A, second: A)(magma: Magma[A]): Unit = {
  println("received: " + first.toString + " " + second.toString)
  val r1 = magma.op(first, second)
  val r2 = magma.op(second, first)
  println("r1: " + r1)
  println("r2: " + r2)
}

function(47, 5)(MagmaInstances.magmaInt)
function("hello", "world")(MagmaInstances.magmaString)
