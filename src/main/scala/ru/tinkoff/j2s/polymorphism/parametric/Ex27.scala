package ru.tinkoff.j2s.polymorphism.parametric

object IdMonad {
  type Id[A] = A

  implicit val idMonad = new Monad[Id] {
    override def pure[A](a: A): Id[A] = a
    override def flatMap[A, B](fa: Id[A])(f: A => Id[B]): Id[B] = f(fa)
  }
}
