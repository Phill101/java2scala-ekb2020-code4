import ru.tinkoff.j2s.polymorphism.parametric.{Magma, MagmaInstances}

def function[A: Magma](first: A, second: A): Unit = {
  println("received: " + first.toString + " " + second.toString)
  val r1 = implicitly[Magma[A]].op(first, second)
  val r2 = implicitly[Magma[A]].op(second, first)
  println("r1: " + r1)
  println("r2: " + r2)
}

implicit val mi = MagmaInstances.magmaInt
implicit val ms = MagmaInstances.magmaString


function(47, 5)
function("s1", "s2")
