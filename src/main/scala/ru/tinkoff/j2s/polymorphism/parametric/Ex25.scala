package ru.tinkoff.j2s.polymorphism.parametric

trait Monad[F[_]] extends Functor[F] {
  // pure(a).flatMap(f) === f(a)
  // fa.flatMap(a => pure(a)) === fa
  // fa.flatMap(f).flatMap(g) === fa.flatMap(a => f(a).flatMap(g))

  def pure[A](a: A): F[A]
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]

  override def map[A, B](fa: F[A])(f: A => B): F[B] =
    flatMap(fa)(f andThen pure)
}

object Monad {
  def apply[F[_]: Monad]: Monad[F] = implicitly

  def monadLeftIdentity[F[_]: Monad, A, B](a: A, f: A => F[B]) =
    Monad[F].pure(a).flatMap(f) == f(a)

  def monadRightIdentity[F[_]: Monad, A](fa: F[A]) =
    fa.flatMap(Monad[F].pure) == fa

  def flatMapAssociativity[F[_]: Monad, A, B, C](fa: F[A], f: A => F[B], g: B => F[C]) =
    fa.flatMap(f).flatMap(g) == fa.flatMap(a => f(a).flatMap(g))

  implicit class FlatMapOps[F[_], A](val fa: F[A]) extends AnyVal {
    def flatMap[B](f: A => F[B])(implicit F: Monad[F]) = F.flatMap(fa)(f)
  }
}
