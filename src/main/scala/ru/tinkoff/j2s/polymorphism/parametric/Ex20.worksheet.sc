import cats.Monoid
import cats.instances.int._
import cats.instances.string._
import cats.syntax.semigroup._

def appendNTimes[A: Monoid](value: A, times: Int): A =
  (1 until times).foldLeft(value) { (x, _) => x |+| value }
  //if (times == 0) Monoid[A].empty
  //else value |+| appendNTimes(value, times - 1)

appendNTimes("ha", 4)
appendNTimes(1337, 42)

Monoid[String].combineN("ha", 3)
Monoid[Int].combineN(1337, 42)
