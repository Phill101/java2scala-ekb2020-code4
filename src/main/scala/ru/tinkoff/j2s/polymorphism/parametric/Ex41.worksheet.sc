import cats._
import cats.data._
import cats.instances.all._
import cats.syntax.all._

def parseIntEither(s: String): Either[NumberFormatException, Int] =
  Either.catchOnly[NumberFormatException](s.toInt)

def parseIntValidated(s: String): ValidatedNel[NumberFormatException, Int] =
  Validated.catchOnly[NumberFormatException](s.toInt).toValidatedNel

List("1", "2", "3", "4").traverse(parseIntEither)
List("1", "abc", "3", "def").traverse(parseIntEither)

List("1", "2", "3", "4").traverse(parseIntValidated)
List("1", "abc", "3", "def").traverse(parseIntValidated)


List(Option(1), Option(2), Option(3)).sequence
List(Option(1), Option(2), None).sequence
