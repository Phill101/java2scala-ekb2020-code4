package ru.tinkoff.j2s.polymorphism.parametric

trait Functor[F[_]] {
  // fa.map(identity) === fa
  // fa.map(f).map(g) === fa.map(f andThen g)
  def map[A, B](fa : F[A])(f: A => B): F[B]
}

object Functor {
  def apply[F[_]: Functor]: Functor[F] = implicitly

  implicit class FunctorOps[F[_], A](val fa: F[A]) extends AnyVal {
    def map[B](f: A => B)(implicit F: Functor[F]) = F.map(fa)(f)
  }

  def identityLaw[F[_]: Functor, A](fa: F[A]) =
    fa.map(identity) == fa

  def compositionLaw[F[_]: Functor, A, B, C](fa: F[A])(f: A => B, g: B => C) =
    fa.map(f).map(g) == fa.map(f andThen g)
}
