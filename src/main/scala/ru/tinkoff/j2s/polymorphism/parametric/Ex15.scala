package ru.tinkoff.j2s.polymorphism.parametric.shadowed

trait Monoid[A] {
  // (x |+| (y |+| z)) === ((x |+| y) |+| z)
  def combine(x: A, y: A): A

  // x combine empty === x
  // empty combine x === x
  def empty: A
}

object Monoid {
  def apply[A](implicit s: Monoid[A]) = s

  def associativity[A](x: A, y: A, z: A)(implicit s: Monoid[A]): Boolean =
    s.combine(x, s.combine(y, z)) == s.combine(s.combine(x, y), z)

  def leftIdentity[A: Monoid](x: A): Boolean  = (x |+| Monoid[A].empty) == x
  def rightIdentity[A: Monoid](x: A): Boolean = (Monoid[A].empty |+| x) == x

  implicit class SemigroupOps[A](val x: A) extends AnyVal {
    def |+|(y: A)(implicit m: Monoid[A]) = m.combine(x, y)
  }
}
