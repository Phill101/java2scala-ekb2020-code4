import cats.instances.int._
import cats.instances.map._
import cats.instances.tuple._
import cats.syntax.semigroup._

trait ResourceType
object Gold extends ResourceType
object Wood extends ResourceType
object Wool extends ResourceType
  
val playerResources = 
  Map(
    Gold -> 500,
    Wood -> 100,
    Wool -> 150
  )


val heavierWay = {
  val newGold = playerResources.getOrElse(Gold, 0) - 100
  val newWool = playerResources.getOrElse(Wool, 0) + 50
  playerResources.updated(Gold, newGold)
                 .updated(Wool, newWool)
}


val updatedPlayerResources1 = playerResources |+| Map(Gold -> -100, Wool -> 50)
val updatedPlayerResources2 = playerResources |+| Map(Gold -> -200, Wool -> 50, Wood -> 100)
val updatedPlayerResources3 = playerResources |+| Map(Gold -> -100, Wool -> 50) |+| Map(Gold -> -100, Wood -> 100)
