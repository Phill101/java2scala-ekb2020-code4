package ru.tinkoff.j2s.polymorphism.parametric

trait Magma[A] {
  def op(x: A, y: A): A
}

object Magma {
  def apply[A](implicit magma: Magma[A]) = magma

  object instances {
    implicit val mi: Magma[Int]    = MagmaInstances.magmaInt
    implicit val ms: Magma[String] = MagmaInstances.magmaString
  }
}
