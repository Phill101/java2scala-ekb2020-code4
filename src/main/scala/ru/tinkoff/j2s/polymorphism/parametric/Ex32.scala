package ru.tinkoff.j2s.polymorphism.parametric

import ru.tinkoff.j2s.polymorphism.parametric.Validated
import ru.tinkoff.j2s.polymorphism.parametric.Validated._

object ValidatedMonad {
  def validatedMonad[E: Semigroup] =
    // val someB: B
    //
    // val f: (A, B) => C
    // val g: A => C = f(_, someB)
    new Monad[Validated[E, *]] {
      override def pure[A](a: A): Validated[E,A] = 
        Valid(a)
      override def flatMap[A, B](fa: Validated[E,A])(f: A => Validated[E,B]): Validated[E,B] = 
        fa match {
          case Valid(a)   => f(a)
          case Invalid(e) => Semigroup[E].combine(e, ???); ???
        }
    }
}
