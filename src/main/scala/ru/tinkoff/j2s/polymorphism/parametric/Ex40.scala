package ru.tinkoff.j2s.polymorphism.parametric

object TraverseInstances {

  implicit val traverseForList: Traverse[List] = new Traverse[List] {
    def traverse[G[_]: Applicative, A, B](fa: List[A])(f: A => G[B]): G[List[B]] =
      fa.foldRight(Applicative[G].pure(List.empty[B])) { (a, acc) =>
        Applicative[G].map2(f(a), acc)(_ :: _)
      }
  }

}
