package ru.tinkoff.j2s.polymorphism.parametric

trait Monoid[A] extends Semigroup[A] {
  // x combine empty === x
  // empty combine x === x
  def empty: A
}

object Monoid {
  import Semigroup._
  def apply[A](implicit s: Monoid[A]) = s

  def leftIdentity[A: Monoid](x: A): Boolean  = (x |+| Monoid[A].empty) == x
  def rightIdentity[A: Monoid](x: A): Boolean = (Monoid[A].empty |+| x) == x
}
