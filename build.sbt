import Dependencies._

ThisBuild / scalaVersion     := "2.13.1"
ThisBuild / version          := "1.0.0"
ThisBuild / organization     := "ru.tinkoff"
ThisBuild / organizationName := "tinkoff.ru"

lazy val root = (project in file("."))
  .settings(
    name := "j2s",
    libraryDependencies += compilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full),
    libraryDependencies += cats.core
  )

