import sbt._

object Dependencies {
  object cats {
    lazy val core   = "org.typelevel" %% "cats-core" % "2.0.0"
  }
}
